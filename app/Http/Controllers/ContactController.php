<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContactRequest;
use App\Http\Services\ContactService;
use Exception;

class ContactController extends Controller
{
    protected $contactService;

    public function __construct(ContactService $contactService)
    {
        $this->contactService = $contactService;
    }

    /**
     * Send Message To Email
     */
    public function send(ContactRequest $request)
    {
        try {
            $contactData = $request->only(['name', 'email', 'phone', 'message']);
            $this->contactService->sendMessage($contactData);

            return response()->json(['message' => 'Email sent successfully'], 200);

        } catch (Exception $exception) {
            return response()->json([
                'error' => true,
                'message' => $exception->getMessage()
            ]);
        }
    }
}

